;;; info-mode-el -- Major mode for editing INFO files

;; Author: Roald Fernandez <contact@swarminglogic.com>
;; Created: 10 Nov 2014
;; Keywords: INFO major-mode

;; Copyright (C) 2014 Roald Fernandez <contact@swarminglogic.com>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2 of
;; the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the implied
;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
;; PURPOSE.  See the GNU General Public License for more details.

;; You should have received a copy of the GNU General Public
;; License along with this program; if not, write to the Free
;; Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
;; MA 02111-1307 USA

;; To use, place a copy of this file in your .emacs.d directory (for
;; example "~/.emacs.d/plugins"), and add the following to your .emacs
;; file:
;;
;;    (add-to-list 'load-path "~/.emacs.d/plugins/")
;;    (require 'generic-x)
;;    (require 'info-mode)
;;
;; This mode will automatically be enabled for file names "*.info"
;;

(defvar info-indent-offset 4
  "Indentation offset for `info-mode'.")

(defun generic-indent-line ()
  "Indent current line for any balanced-paren-mode'."
  (interactive)
  (let ((indent-col 0)
        (indentation-increasers "[{]")
        (indentation-decreasers "[}]")
        )
    (save-excursion
      (beginning-of-line)
      (condition-case nil
          (while t
            (backward-up-list 1)
            (when (looking-at indentation-increasers)
              (setq indent-col (+ indent-col info-indent-offset))))
        (error nil)))
    (save-excursion
      (back-to-indentation)
      (when (and (looking-at indentation-decreasers) (>= indent-col info-indent-offset))
        (setq indent-col (- indent-col info-indent-offset))))
    (indent-line-to indent-col)))

(define-generic-mode 'info-mode
  '(";")
  '("true" "false")
  '(("^#include" . 'font-lock-preprocessor-face)
    ("[[:space:]][0-9\\.]+f?" . 'font-lock-variable-name-face)
    ("^[[:space:]]*\\([-_A-Za-z0-9]+\\)" 1 'font-lock-constant-face))
  '("\\.info$")
  '((lambda ()
      (make-local-variable 'info-indent-offset)
      (make-local-variable 'generic-indent-line)
      (set 'indent-line-function 'generic-indent-line)
      ))
  "A mode for info files"
  )

(provide 'info-mode)

;;; info-mode.el ends here
