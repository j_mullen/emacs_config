;; init.el
;;
;; Copyright (C) 2012-2014 James Mullen
;;
;; Init.el is free software: you can redistribute it and/or modify it under the
;; terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; Init.el is distributed in the hope that it will be useful, but WITHOUT ANY
;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
;; A PARTICULAR PURPOSE. See the GNU General Public License for more details.
;;
;; You should have recieved a copy of the GNU General Public License along with
;; this program. If not, see <http://www.gnu.org/licenses/>.
;;
;;
;; FILE HISTORY
;;
;; Date             Comment
;; -----------      ---------------------------------------------------------
;; 07-Jul-2014      Added file history (finally).
;; 07-Jul-2014      Elaborated on many of the documented entries.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Load Paths ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Everything under ~/.emacs.d/lisp will be loaded recursively.
(let ((default-directory "~/.emacs.d/lisp/"))
  (normal-top-level-add-to-load-path '("."))
  (normal-top-level-add-subdirs-to-load-path))
;; Themes
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

;; General ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; General preferences for Emacs are located here. When adding more options in
;; the future, be sure to comment what each does.
(show-paren-mode 1) ;; Show matching parenthesis
(menu-bar-mode t) ;; Show the menu
(tool-bar-mode -1) ;; Turn off toolbar
(setq
 scroll-margin 0 ;; smooth scrolling
 scroll-conservatively 100000
 scroll-up-agressively 0
 scroll-down-agressively 0
 scroll-preserve-screen-position t) ;; Preserve screen pos with C-v/M-v
(setq search-highlight t ;; Highlight when searching
      query-replace-highlight t) ;; ...and replacing
(setq inhibit-startup-message t) ;; Hide welcome

;; Keyboard ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Keyboard options and macros are located here. Specifically, a fix for one
;; irritation: the home and end keys move to the beginning and end of the
;; buffer on Mac OS. They should now emulate Windows and Linux.
(cond
 ((string-equal system-type "darwin") ;; Mac OS
  (global-set-key (kbd "<home>") 'move-beginning-of-line)
  (global-set-key (kbd "<end>") 'move-end-of-line)
  )
 )
(global-set-key (kbd "C-<f7>") 'compile) ;; Ctrl-F7 to compile
(global-set-key (kbd "C-<f8>") 'comment-or-uncomment-region) ;; (Un)comment

;; Modeline ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq line-number-mode t)     ;; Always show the line number
(setq column-number-mode t)   ;; Always show the column number
(setq size-indication-mode t) ;; Always show the size of the buffer

;; Autoloading ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Place any major-mode extensions here.
;;
;; Idutils is a GNU utility that allows for fast lookup of tokenized identifiers
;; in many different programming languages. It will, of course, need to be
;; installed for idutils mode to work.
(autoload 'gid "idutils" "run idutils' gid command" t) ;; gid within Emacs
(autoload 'cmake-mode "cmake-mode" nil t) ;; CMake major mode
(autoload 'glsl-mode "glsl-mode" nil t) ;; GLSL major mode
(autoload 'info-mode "info-mode" nil t) ;; Boost Property Tree mode
(require 'google-c-style)
(add-hook 'c-mode-common-hook 'google-set-c-style)

;; Color and Themes ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;(load-theme 'leuven t)
(load-theme 'zenburn t)

;; Fonts ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Font selection is done in order of preference, with the old standby, Courier,
;; being the last choice. Terminus is preferred due to readability and
;; comprehension at all sizes. Terminus can be installed either locally on
;; Windows, in a Cygwin environment, or natively in a Linux environment. The
;; preferred font choice for Mac OS is Inconsolata, which ships with the OS.
;;
;; The Terminus font and its installation instructions can be found at the
;; the following URL: http://terminus-font.sourceforge.net/
;;
;; NOTE: In Cygwin/X, the font path will need to be added to the file
;; '.startwinrc' in the user's home directory. This requires xset and xrdb to
;; be installed. Add something like the following:
;;
;; xset fp+ <path-to-terminus>
;; xset fp rehash
(cond
 ((find-font (font-spec :name "Terminus"))
  (set-frame-font "Terminus-12"))
 ((find-font (font-spec :name "Inconsolata"))
  (set-frame-font "Inconsolata-14"))
 ((find-font (font-spec :name "Lucida Console"))
  (set-frame-font "Lucida Console-12"))
 ((find-font (font-spec :name "Courier"))
  (set-frame-font "Courier-10"))
)
(setq font-lock-maximum-decoration t) ;; Maximum syntax highlight (fruitloops)

;; Text ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Text formatting and indentation style for c/c++-mode is set to use spaces
;; instead of tabs, with the default offset being "Linux" style (4 spaces).
;;
;; Autopair is a bracket-completion minor mode that works with many of Emacs'
;; major modes and automatically inserts and deletes matching braces. It is
;; currently 'disabled' until I can figure out how to avoid the GNU-style brace
;; indentation.
(setq-default indent-tabs-mode nil) ;; No tabs
(setq c-default-style "linux" c-basic-offset 2) ;; Set indentation style
;;(require 'autopair) ;; Automatically pair braces
;;(autopair-global-mode 1)
;;(setq autopair-autowrap t)
(add-hook 'c-mode-common-hook 'google-set-c-style)

;; show-paren-mode Color and Face ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This changes the color and face of highlighted parenthesis to the color and
;; weight defined below. This is to add extra visibiity to matching parens. If
;; the color of the highlight doesn't work with your theme, then change the
;; value of paren-bg-color. Colors can be listed by executing
;; 'M-x list-colors-display' or can be in hexadecimal RGB '#RRGGBB'.
(require 'paren)
(defvar paren-bg-color "DarkOrange1") ;; Highlight color
(set-face-background 'show-paren-match-face (face-background 'default))
(set-face-foreground 'show-paren-match-face "#def")
(set-face-attribute 'show-paren-match-face nil :weight 'ultra-bold)
(set-face-attribute 'show-paren-match-face nil :background paren-bg-color)

;; Backups ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Backups are stored in the subdirectory ~/.emacs.d/backups.
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")))
(setq delete-old-versions t
    kept-new-versions 6
    kept-old-versions 2
    version-control t)

;; Auto Mode ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; The following section defines what major modes to enter based on the file
;; being opened.
(setq auto-mode-alist
      (append
       '(("CMakeLists\\.txt\\'" . cmake-mode))
       '(("\\.cmake\\'" . cmake-mode))
       '(("\\.glsl\\'" . glsl-mode))
       '(("\\.vert\\'" . glsl-mode))
       '(("\\.frag\\'" . glsl-mode))
       '(("\\.geom\\'" . glsl-mode))
       '(("\\.vs\\'" . glsl-mode))
       '(("\\.fs\\'" . glsl-mode))
       '(("\\.gs\\'" . glsl-mode))
       auto-mode-alist))
(put 'erase-buffer 'disabled nil)


;; Ediff ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq ediff-diff-options "-w") ;; Ignore whitespace changes

;; Compilation ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq compilation-scroll-output 'first-error) ;; Stop scroll on first error

;; Shell ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-hook 'comint-output-filter-functions
          'comint-watch-for-password-prompt)
